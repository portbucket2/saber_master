﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpwaner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public int enemynumber;
    public Vector3[] positions;
    void Start()
    {
        for (int i = 0; i < enemynumber; i++)
        {
            GameObject gg = Instantiate(enemyPrefab, positions[i], enemyPrefab.transform.rotation, transform);
            gg.GetComponent<EnemyController>().MakeARunner();
        }
    }

    
}
