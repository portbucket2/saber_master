﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ScrollEnvironment : MonoBehaviour
{
    [SerializeField] List<Transform> landMasses;
    [SerializeField] float unitDistance = 2f;
    [SerializeField] float moveRate = 1f;
    [SerializeField] Transform jets;

    [Header("Auto Fill, For Debug only:")]
    [SerializeField] int max ;
    [SerializeField] Vector3 unitDistanceV;
    [SerializeField] Vector3 spwanPoint;
    [SerializeField] Vector3 endPoint;
    [SerializeField] List<Vector3> landMassesPos;

    

    void Start()
    {
        landMassesPos = new List<Vector3>();
        unitDistanceV = new Vector3(0f, 0f, unitDistance);
        max = landMasses.Count;
        for (int i = 0; i < max; i++)
        {
            landMassesPos.Add(unitDistanceV * i);
        }
        spwanPoint = landMassesPos[max - 1];
        endPoint = unitDistanceV * -2f;
        for (int i = 0; i < max; i++)
        {
            landMasses[i].transform.localPosition = unitDistanceV * i;
                //new Vector3(landMasses[i].transform.localPosition.x, landMasses[i].transform.localPosition.y,unitDistance * i);
        }
        for (int i = 0; i < max; i++)
        {
            StartScrolling(landMasses[i]);
        }
        JetsMove();
    }

    
    void StartScrolling(Transform _transform)
    {
        //_transform.DOKill(true);
        _transform.DOLocalMove(endPoint, GetDuration(moveRate, _transform.localPosition, endPoint)).SetEase(Ease.Linear).OnComplete(
            ()=> {
                _transform.localPosition = spwanPoint;
                StartScrolling(_transform);
            });
    }

    float GetDuration(float rate, Vector3 start, Vector3 end)
    {
        float dist = Vector3.Distance(start, end);
        float duration = (float)dist / rate;
        //Debug.LogError("move duration: " + duration);
        return duration;
    }

    void JetsMove()
    {
        jets.DOLocalMove(endPoint, 2f).SetEase(Ease.Linear).OnComplete(
            () => {
                jets.localPosition = spwanPoint;
                //JetsMove();
                StartCoroutine(JetsRoutine());
            });
    }
    IEnumerator JetsRoutine()
    {
        yield return new WaitForSeconds(Random.Range(3, 7));
        JetsMove();
    }
}
