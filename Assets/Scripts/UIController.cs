﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{
    [Header("Common")]
    [SerializeField] GameObject restartPanel;
    [SerializeField] GameObject nextLevelPanel;
    [SerializeField] Button nextLevelButton;
    [SerializeField] Button restartButton;
    [SerializeField] TextMeshProUGUI levelCount;

    [Header("Weilding Menu buttons")]
    [SerializeField] Button toggleMenu;
    [SerializeField] Button oneHanded;
    [SerializeField] Button twoHanded;
    [SerializeField] Button dualWeilding;
    [SerializeField] bool isMenuOn = false;
    [SerializeField] GameObject menuPanel;

    [Header("Power button")]
    [SerializeField] Button powerButton;
    [SerializeField] Animator powerButtonAnimator;
    [SerializeField] Image fillImage;
    [SerializeField] Image fillImage2;
    [SerializeField] float fillingTime = 1f;
    [SerializeField] float perSlash = 0.2f;

    GameController gameController;
    PlayerController playerController;

    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    [Header("For Debug Values")]
    [SerializeField] float currentValue = 0f;
    [SerializeField] float previousValue = 0f;

    readonly string ACTIVE = "active";
    readonly string INACTIVE= "inActive";

    GameManager gameManager;
    AnalyticsController analytics;

    void Start()
    {
        gameManager = GameManager.GetManager();
        gameController = GameController.GetController();
        playerController = gameController.GetPlayerController();
        analytics = AnalyticsController.GetController();
        powerButton.onClick.AddListener(delegate
        {
            PowerMeter(0f);
            powerButton.interactable = false;
            powerButtonAnimator.SetTrigger(INACTIVE);
            //Debug.Log("power used!!");
            gameController.SaberSlam();
        });
        oneHanded.onClick.AddListener(delegate
        {
            playerController.ChangeWielding(WeildingStyle.ONEHANDED);
            WeildingMenu();
        });
        twoHanded.onClick.AddListener(delegate
        {
            playerController.ChangeWielding(WeildingStyle.TWOHANDED);
            WeildingMenu();
        });
        dualWeilding.onClick.AddListener(delegate
        {
            playerController.ChangeWielding(WeildingStyle.DUALWEILD);
            WeildingMenu();
        });
        toggleMenu.onClick.AddListener(delegate
        {
            WeildingMenu();
                
        });
        restartButton.onClick.AddListener(delegate {
            gameManager.ResetStage();
        });
        nextLevelButton.onClick.AddListener(delegate {
            gameManager.GotoNextStage();
            analytics.LevelEnded();
        });
        levelCount.text = "Level " + (gameManager.GetDataManager().GetGamePlayer.levelsCompleted ) ;
    }
    

    void WeildingMenu()
    {
        if (isMenuOn)
        {
            isMenuOn = false;
            menuPanel.SetActive(false);
        }
        else
        {
            isMenuOn = true;
            menuPanel.SetActive(true);
        }
    }
    void PowerMeter(float _targetValue)
    {
        StartCoroutine(PowerMeterCoolDown(_targetValue));
    }
    IEnumerator PowerMeterCoolDown(float targetValue)
    {
        currentValue = fillImage.fillAmount;
        currentValue = fillImage2.fillAmount;
        previousValue = targetValue;
        float elapsedTime = 0.01f;
        float waitTime = fillingTime;
        while (elapsedTime < waitTime)
        {
            //currentValue += Time.deltaTime * fillSpeed;
            fillImage.fillAmount = Mathf.SmoothStep(currentValue, targetValue, (elapsedTime/waitTime));
            fillImage2.fillAmount = Mathf.SmoothStep(currentValue, targetValue, (elapsedTime/waitTime));
            elapsedTime += Time.deltaTime;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        fillImage.fillAmount = targetValue;
        fillImage2.fillAmount = targetValue;
        //Debug.Log("out!!");
        if (previousValue > 0.98f)
        {
            // finished
            //Debug.Log("Weapons Ready!!");
            powerButton.interactable = true;
            powerButtonAnimator.SetTrigger(ACTIVE);
        }
        else
        {
            powerButton.interactable = false;
            powerButtonAnimator.SetTrigger(INACTIVE);
        }

    }
    public void AddPowerMeter()
    {
        PowerMeter(previousValue + perSlash);
    }
    public GameObject NextLevelPanel()
    {
        return nextLevelPanel;
    }
    public GameObject RestartPanel()
    {
        return restartPanel;
    }
}
