﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField] bool doorUnlocked = true;
    [SerializeField] Animator animator;

    readonly string OPEN = "open";
    readonly string CLOSE = "close";

    readonly string PLAYER = "Player";

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(PLAYER) && doorUnlocked)
        {
            animator.SetTrigger(OPEN);
        }
    }
}
