﻿/*
    Collection of classes and structs here
 */


using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GamePlayer 
{
    public string name;
    public int id;
    public int levelsCompleted;
    public int totalStars;
    public int lastPlayedLevel;
    public bool handTutorialShown = false;
}

