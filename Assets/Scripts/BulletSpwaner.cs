﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpwaner : MonoBehaviour
{
    [SerializeField] bool isManed = false;
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] float delay = 2f;
    [SerializeField] Transform shooter;

    [Header("is Bot")]
    [SerializeField] Transform botHead;
    [SerializeField] float followDistance = 30f;
    [SerializeField] DestructibleObject destructible;
    [SerializeField] ParticleSystem destructionParticle;
    [SerializeField] ParticleSystem hitParticle;

    [Header("health")]
    [SerializeField] float currentHealth = 5f;
    [SerializeField] float maxHealth = 5f;

    [Header("Auto Fill")]
    [SerializeField] Transform player;
    [SerializeField] EnemyController enemyController;
    [SerializeField] CapsuleCollider collider;
    [SerializeField] GameObject shieldObject;

    [SerializeField] bool isInRange = false;
    [SerializeField] bool isAlive = true;

    Vector3 offset;
    GameController gameController;

    readonly string LIGHTSABER = "lightSaber";
    readonly string BULLET = "bullet";

    WaitForSeconds DELAY;
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();


    void Start()
    {
        gameController = GameController.GetController();
        if (isManed) { enemyController = transform.parent.GetComponent<EnemyController>(); }
        else { shieldObject = transform.GetChild(0).gameObject; }
        DELAY = new WaitForSeconds(delay);
        offset = new Vector3(0f, transform.position.y, 0f);
        player = gameController.GetPlayerController().transform;
        collider = GetComponent<CapsuleCollider>();
        StartShooting();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(LIGHTSABER) && !isManed)
        {
            // do shit
            OnDeath();
            //gameObject.SetActive(false);
        }else if (other.transform.CompareTag(BULLET) && !isManed)
        {
            // do shit
            if (other.GetComponent<Bullet>().isReturning)
            {
                hitParticle.transform.position = other.transform.position;
                hitParticle.Play();
                currentHealth--;
                if (currentHealth <= 0)
                {
                    OnDeath();
                }
            }
            //gameObject.SetActive(false);
        }
    }
    private void Update()
    {
        if (isAlive)
        {
            if (Vector3.Distance(transform.position, player.position) < followDistance)
            {
                if (!isManed)
                {
                    botHead.LookAt(player);
                }
                isInRange = true;
            }
            else
            {
                isInRange = false;
            }
        }
    }
    void OnDeath()
    {
        destructionParticle.Play();
        destructible.DestroyBot();
        collider.enabled = false;
        shieldObject.SetActive(false);
        StopAllCoroutines();
        isAlive = false;
    }
    void StartShooting()
    {
        StartCoroutine(ShooterRoutine());
    }

    IEnumerator ShooterRoutine()
    {
        yield return WAITONE;
        Shoot();
        yield return DELAY;
        yield return ENDOFFRAME;
        StartShooting();
    }

    void Shoot()
    {
        if (isInRange)
        {
            if (isManed)
            {
                enemyController.ShootAnimation();
            }
            else
                botHead.LookAt(player);

            GameObject gg = Instantiate(bulletPrefab, transform.position, bulletPrefab.transform.rotation);
            Bullet bb = gg.GetComponent<Bullet>();
            if (isManed)
            {
                Vector3 pos = new Vector3(shooter.parent.position.x, shooter.position.y, shooter.parent.position.z);
                bb.SetBullet(pos, player.position + offset);
            }
            else
                bb.SetBullet(shooter.position, player.position + offset);
        }
    }

    
}
