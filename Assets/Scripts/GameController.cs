﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;


    [SerializeField] PlayerController playerController;
    [SerializeField] InputManager inputManager;
    [SerializeField] UIController uIController;
    [SerializeField] VariableJoystick variableJoystick;

    [Header("For Debugging")]

    [SerializeField] GameManager gameManager;


    private void Awake()
    {
        gameController = this;        
    }

    void Start()
    {
        gameManager = GameManager.GetManager();
    }

    public static GameController GetController()
    {
        return gameController;
    }
    public PlayerController GetPlayerController()
    {
        return playerController;
    }
    public UIController GetUIController()
    {
        return uIController;
    }
    public VariableJoystick GetVariableJoystick()
    {
        return variableJoystick;
    }

    public void AddPowerMeter()
    {
        uIController.AddPowerMeter();
    }
    public void SaberSlam()
    {
        playerController.SaberSlam();
    }
}
