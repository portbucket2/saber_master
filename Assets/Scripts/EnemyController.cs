﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public int enemyNumber = 0;
    [SerializeField]private bool isRunner = false;
    [SerializeField]private float pushForce = 100f;
    [SerializeField]private Animator animator;
    [SerializeField]private Rigidbody spineRb;
    //[SerializeField]private GameObject spineCutMark;

    [SerializeField]private Color fullHealthColor;
    [SerializeField]private Color deadColor;
    [SerializeField]private GameObject bulletSpwaner;
    [SerializeField]private GameObject gun;
    //[SerializeField] private SkinnedMeshRenderer renderer;


    [Header("Enemy Health:")]
    [SerializeField]private int currentHealth;
    [SerializeField]private int maxHealth;
    [SerializeField]private Color currentHealthColor;
    [SerializeField]private Material deadEnemyMat;
    [SerializeField]private Material deadEnemyGlowMat;
    [SerializeField] Transform skinnedMeshParent;

    [Header("Enemy Diifertent colors:")]
    [SerializeField] private GameObject torsoBlue;
    [SerializeField] private GameObject legBlue;
    [SerializeField] private GameObject torsoRed;
    [SerializeField] private GameObject legRed;
    [SerializeField] private GameObject upModelBlue;
    [SerializeField] private GameObject downModelBlue;
    [SerializeField] private GameObject upModelRed;
    [SerializeField] private GameObject downModelRed;
    [SerializeField] private Rigidbody cutBody2;
    [SerializeField] private Rigidbody cutBody3;



    [Header("Privates for Debug:")]
    [SerializeField] private bool PlayerDetected = false;
    [SerializeField] private GameObject upModel;
    [SerializeField] private GameObject downModel;
    [SerializeField] List<SkinnedMeshRenderer> skinnedMeshes;

    private bool isAttacking = false;
    Transform playerTransform;

    NavMeshAgent agent;
    GameController gameController;

    CapsuleCollider capsule;
    Vector3 playerPos;
    //  Tags
    readonly string LIGHTSABER = "lightSaber";
    readonly string BULLET = "bullet";
    readonly string PLAYER = "Player";

    // animation stings
    readonly string IDLE = "idle";
    readonly string WALK = "walk";
    readonly string HIT = "hit";
    readonly string PUNCH = "punch";
    readonly string SHOOT = "shoot";
    readonly string MOVESPEED = "moveSpeed";

    WaitForSeconds WAITONE = new WaitForSeconds(0.5f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    private void Awake()
    {
        //controller = this;
    }

    private void Start()
    {
        gameController = GameController.GetController();
        capsule = GetComponent<CapsuleCollider>();
        agent = GetComponent<NavMeshAgent>();
        playerTransform = gameController.GetPlayerController().transform;
        if (isRunner)
        {
            agent.SetDestination(transform.position);
            animator.SetTrigger(IDLE);
            bulletSpwaner.SetActive(false);
            gun.SetActive(false);
            torsoBlue.SetActive(true);
            legBlue.SetActive(true);
            torsoRed.SetActive(false);
            legRed.SetActive(false);
            upModel = upModelBlue;
            downModel = downModelBlue;
        }
        else
        {
            agent.SetDestination(transform.position);
            animator.SetTrigger(IDLE);
            bulletSpwaner.SetActive(true);
            gun.SetActive(true);
            torsoBlue.SetActive(false);
            legBlue.SetActive(false);
            torsoRed.SetActive(true);
            legRed.SetActive(true);
            upModel = upModelRed;
            downModel = downModelRed;
        }

        int max = skinnedMeshParent.childCount;
        skinnedMeshes = new List<SkinnedMeshRenderer>();
        for (int i = 0; i < max; i++)
        {
            skinnedMeshes.Add(skinnedMeshParent.GetChild(i).GetComponent<SkinnedMeshRenderer>());
        }

    }
    private void Update()
    {
        playerPos = playerTransform.position;
        if (!PlayerDetected && Vector3.Distance(transform.position, playerPos) < 40f) 
        {
            PlayerDetected = true;
        }
        if (isRunner && agent.isActiveAndEnabled && PlayerDetected)
        {
            
            agent.SetDestination(playerPos);
            FaceTarget(playerPos);

            float dis = Vector3.Distance(transform.position, playerPos);
            if (dis < 2f)
            {
                MovementSpeed();
                agent.SetDestination(transform.position);
                FaceTarget(playerPos);
                
            }
            else
            {
                MovementSpeed();
                isAttacking = false;
                agent.SetDestination(playerPos);
                FaceTarget(playerPos);
            }
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(LIGHTSABER))
        {
            // do shit
            if (!IsAlive())
            {
                capsule.enabled = false;
                animator.enabled = false;
                //Debug.Log("collided");
                Vector3 dir = transform.position - other.transform.position;
                
                upModel.SetActive(false);
                downModel.SetActive(true);

                spineRb.AddForce((dir + Vector3.up) * pushForce, ForceMode.Impulse);
                cutBody2.AddForce((dir + Vector3.up) * pushForce, ForceMode.Impulse);
                cutBody3.AddForce((dir + Vector3.up) * pushForce, ForceMode.Impulse);
                //spineCutMark.SetActive(true);

                gameController.AddPowerMeter();

                agent.SetDestination(transform.position);
                agent.enabled = false;
            }
        }else if (other.transform.CompareTag(BULLET))
        {
            // do shit
            if (other.GetComponent<Bullet>().isReturning)
            {
                if (!IsAlive())
                {
                    capsule.enabled = false;
                    animator.enabled = false;
                    //Debug.Log("shot");
                    other.GetComponent<Bullet>().OnComplete();
                    Vector3 dir = transform.position - other.transform.position;
                    spineRb.AddForce((dir + Vector3.up) * pushForce , ForceMode.Impulse);
                    //spineCutMark.SetActive(true);

                    agent.SetDestination(transform.position);
                    agent.enabled = false;
                }
            }
        }
        else if (other.transform.CompareTag(PLAYER))
        {
            // do shit
            if (!isAttacking)
            {
                //Debug.Log("attacked");
                //StartCoroutine(AttackPlayerRoutine());
                AttackPlayer();
            }
        }
    }
    void MovementSpeed()
    {
        float speed = agent.speed;
        if (speed > 1) { speed = 1f; }
        animator.SetFloat(MOVESPEED, speed);
    }
    public void MakeARunner()
    {
        isRunner = true;
    }
    public void ForcePushed()
    {
        if (!IsAlive())
        {
            capsule.enabled = false;
            animator.enabled = false;
            //Debug.Log("collided");
            Vector3 dir = transform.position - gameController.GetPlayerController().transform.position;
            Vector3 dir2;
            if (transform.position.x > 0) { dir2 = Vector3.right; }
            else { dir2 = Vector3.left; }

            spineRb.AddForce((dir + dir2 *4f) * pushForce * 1.2f, ForceMode.Impulse);
            //spineCutMark.SetActive(true);

            agent.SetDestination(transform.position);
            agent.enabled = false;
        }
    }
    public void ForceSlamed()
    {
        if (!IsAlive())
        {
            capsule.enabled = false;
            animator.enabled = false;
            //Debug.Log("collided");
            Vector3 dir = transform.position - gameController.GetPlayerController().transform.position;
            Vector3 dir2;
            if (transform.position.x > 0) { dir2 = Vector3.right; }
            else { dir2 = Vector3.left; }

            spineRb.AddForce(dir2 * pushForce * 2f, ForceMode.Impulse);
            //spineCutMark.SetActive(true);

            agent.SetDestination(transform.position);
            agent.enabled = false;
        }
    }
    public void ShootAnimation()
    {
        FaceTarget(playerTransform.position);
        animator.SetTrigger(SHOOT);
    }
    bool IsAlive()
    {
        currentHealth--;
        bool alive = true;
        SetColor();
        if (currentHealth > 0) 
        {
            // death animation
            alive = true;
            animator.SetTrigger(HIT);
            StartCoroutine(MovementPause());
        }
        else
        {
            //show hit animation
            alive = false;
            StopAllCoroutines();
            bulletSpwaner.SetActive(false);
        }
        return alive;
    }

    IEnumerator MovementPause()
    {
        if (agent.isActiveAndEnabled)
        {
            agent.isStopped = true;
            yield return WAITONE;
            agent.isStopped = false;
            if (isRunner) { animator.SetTrigger(WALK); }
        }
    }
    void SetColor()
    {
        int max = skinnedMeshParent.childCount;
        

        float temp = (float)currentHealth / maxHealth;

        if (currentHealth <= 0)
        {
            for (int i = 0; i < max; i++)
            {
                Material[] mats = new Material[] { deadEnemyMat, deadEnemyGlowMat };
                skinnedMeshes[i].materials = mats;
            }
        }
    }
    private void FaceTarget(Vector3 destination)
    {
        Vector3 lookPos = destination - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 1f);
    }
    void AttackPlayer()
    {
        isAttacking = true;
        animator.SetTrigger(PUNCH);
        gameController.GetPlayerController().OnEnemyAttack();
        
    }
    IEnumerator AttackPlayerRoutine()
    {
        AttackPlayer();
        yield return WAITONE;
        AttackPlayer();
    }
}
