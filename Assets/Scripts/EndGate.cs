﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGate : MonoBehaviour
{
    GameController gameController;
    AnalyticsController analytics;
    readonly string PLAYER = "Player";
    void Start()
    {
        gameController = GameController.GetController();
        analytics = AnalyticsController.GetController();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(PLAYER))
        {
            // do shit
            gameController.GetUIController().NextLevelPanel().SetActive(true);
            
        }
    }
}
