﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialScript : MonoBehaviour
{
    [SerializeField] Button tapToStart;

    GameManager gameManager;
    void Start()
    {
        gameManager = GameManager.GetManager();
        if (gameManager.TutorialAlreadySeen())
        {
            gameObject.SetActive(false);
        }
        tapToStart.onClick.AddListener(delegate {
            gameObject.SetActive(false);
            gameManager.TutorialSeen();
        });
        
    }

    
}
