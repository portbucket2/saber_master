﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bullet : MonoBehaviour
{
    [Header("For Debug")]
    public bool isReturning = false;
    [SerializeField] Vector3 shooter;
    [SerializeField] Vector3 player;
    void Start()
    {
        
    }


    public void KillPlayer(Vector3 position)
    {
        float dis = Vector3.Distance(transform.position, position);

        transform.DOLocalMove(position, dis / 20f).SetEase(Ease.Flash).OnComplete(OnComplete);
    }

    public void KillShooter()
    {
        isReturning = true;
        float dis = Vector3.Distance(transform.position, shooter);
        transform.DOKill();
        transform.DOLocalMove(shooter , dis / 50f).SetEase(Ease.Flash).OnComplete(OnComplete);
    }
    public void SetBullet(Vector3 _shooter, Vector3 _player)
    {
        shooter = _shooter;
        player = _player;
        KillPlayer(_player);
    }
    public void OnComplete()
    {
        //transform.DOKill();
        gameObject.SetActive(false);
    }
}
