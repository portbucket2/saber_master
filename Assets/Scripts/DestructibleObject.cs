﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleObject : MonoBehaviour
{
    [SerializeField] float explosionForce = 200f;
    [SerializeField] List<Rigidbody> rigids;
    [SerializeField] List<GameObject> disintegrate;
    void Start()
    {
        
    }

    public void DestroyBot()
    {
        int max = rigids.Count;
        for (int i = 0; i < max; i++)
        {
            rigids[i].isKinematic = false;
            rigids[i].AddForce(Random.onUnitSphere * explosionForce);

        }

        int min = disintegrate.Count;
        for (int i = 0; i < min; i++)
        {
            disintegrate[i].SetActive(false);
        }
    }
}
