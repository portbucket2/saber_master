﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    [SerializeField] bool isTwohanded = false;
    [SerializeField] bool isAnticipationOn = true;
    [SerializeField] float speed;
    [SerializeField] VariableJoystick variableJoystick;
    [SerializeField] Animator animator;
    [SerializeField] Transform saber1;
    [SerializeField] Transform saber2;
    [SerializeField] CapsuleCollider saberCollider1;
    [SerializeField] CapsuleCollider saberCollider2;
    [SerializeField] float swipeDelay = 0.6f;
    [SerializeField] float brakeDelay = 0.5f;
    [SerializeField] NavMeshAgent agent;
    [SerializeField] float deflectRadius = 3f;
    [SerializeField] float pushRadius = 12f;
    [SerializeField] float slamRadius = 16f;
    [SerializeField] ParticleSystem forcePush;
    [SerializeField] ParticleSystem saberSlam;
    [SerializeField] ParticleSystem deflect;
    [SerializeField] ParticleSystem earthShatter;
    [SerializeField] Volume volume;
    [SerializeField] Lightsaber lightsaber1;
    [SerializeField] Lightsaber lightsaber2;
    [SerializeField] LightsaberTrail lightsaberTrail1;
    [SerializeField] LightsaberTrail lightsaberTrail2;
    [SerializeField] Camera playerCam;
    [SerializeField] Transform lookAtTarget;

    



    [Header("Player Health: ")]
    [SerializeField] float currentHealth = 5f;
    [SerializeField] float maxHealth = 5f;


    [Header("For Debug: ")]
    [SerializeField] bool isplayerStanding = true;
    [SerializeField] bool isJoystickEnabled = false;
    [SerializeField] VolumeProfile volumeProfile = null;
    [SerializeField] WeildingStyle weildingStyle;
    [SerializeField] Animator cameraAnimator;
    [SerializeField] float horizontalValue;

    Vignette vignette = null;

    
    Vector3 movement;

    readonly string BULLET = "bullet";
    readonly string ENEMY = "enemy";
    readonly string LEFT = "left";
    readonly string RIGHT = "right";

    string ONEHANDEDIDLE = "idle";
    string ONEHANDEDSLASHL = "slashL";
    string ONEHANDEDSLASHR = "slashR";
    string ONEHANDEDSLASHF = "slashF";

    string TWOHANDEDIDLE = "twoHandedIdle";
    string TWOHANDEDLEFT = "twoHandedLeft";
    string TWOHANDEDRIGHT = "twohandedRight";
    string TWOHANDEDFRONT = "twohandedFront";


    string NA_SH_RS = "NA_SH_RS";
    string NA_SH_LS = "NA_SH_LS";
    
    string NA_TH_RS = "NA_TH_RS";
    string NA_TH_LS = "NA_TH_LS";

    string DW_IDLE = "dualIdle";
    string DW_LS = "dualLeftSlash";
    string DW_RS = "dualRightSlash";
    string DW_FP = "dualForcePush";
    string DW_DS = "dualDownSlash";


    string IDLE = "idle";
    string SLASHL = "slashL";
    string SLASHR = "slashR";
    string SLASHF = "slashF";
    string FORCEPUSH = "forcePush";


    WaitForSeconds WAITONE = new WaitForSeconds(0.5f);
    WaitForSeconds SLAMWAIT = new WaitForSeconds(0.3f);

    float temp = 0f;
    float brakingTime = 0f;
    GameController gameController;
    GameManager gameManager;
    AnalyticsController analytics;

    Vector3 next;
    Vector3 prev;
    void Start()
    {
        Application.targetFrameRate = 60;
        gameController = GameController.GetController();        
        variableJoystick = gameController.GetVariableJoystick();

        gameManager = GameManager.GetManager();
        analytics = AnalyticsController.GetController();
        weildingStyle = gameManager.GetWeildingStyle();
        analytics.LevelStarted();

        ChangeWielding(weildingStyle);
        Buttons();
        volumeProfile = volume.sharedProfile;
        volumeProfile.TryGet(out vignette);
        vignette.active = false;
        lightsaber1 = saber1.GetChild(0).GetComponent<Lightsaber>();
        lightsaber2 = saber2.GetChild(0).GetComponent<Lightsaber>();

        animator.SetTrigger(IDLE);
        cameraAnimator = playerCam.GetComponent<Animator>();
    }
    bool isBrakeOn = true;
    void Update()
    {
        //if (!isBrakeOn)
        //{
        //    brakingTime += Time.deltaTime;
        //    if (brakingTime > brakeDelay)
        //    {
        //        isBrakeOn = true;
        //        agent.autoBraking = true;
        //    }
        //}
        if (!isJoystickEnabled)
        {
            temp += Time.deltaTime;
            if (temp > swipeDelay)
            {
                isJoystickEnabled = true;
            }
        }
        if (isJoystickEnabled)
        {
            //Vector3 direction = Vector3.forward * variableJoystick.Vertical * speed + Vector3.right * variableJoystick.Horizontal * speed;
            //transform.position += direction;
            
            float horizontalInput = variableJoystick.Horizontal;// Input.GetAxisRaw("Horizontal");
            float verticalInput = variableJoystick.Vertical;// Input.GetAxisRaw("Vertical");

            movement.Set(horizontalInput, 0f, verticalInput);

            agent.Move(movement * Time.deltaTime * agent.speed);
            agent.SetDestination(transform.position + movement);

            if (isplayerStanding && (variableJoystick.Vertical > 0 || variableJoystick.Horizontal > 0))
            {
                isplayerStanding = false;
                //animator.SetTrigger(WALK);
            }

            next = new Vector3(0f, 0f, lookAtTarget.position.z);

            //Vector3 velocity = Vector3.zero;
            //transform.LookAt(Vector3.SmoothDamp(prev, next, ref velocity, 0.2f));
            //prev = next;

            transform.DOLookAt(next, 0.2f).SetEase(Ease.Linear);

        }

        if (Input.GetMouseButtonDown(0))
        {
            horizontalValue = Input.mousePosition.x;
            //agent.autoBraking = false;
            //isBrakeOn = true;
        }
        if (Input.GetMouseButton(0))
        {
            
        }
        if (Input.GetMouseButtonUp(0))
        {
            isplayerStanding = true;
            isJoystickEnabled = false;
            brakingTime = 0f;
            CameraShake(Input.mousePosition.x);
            //agent.autoBraking = true;

            //isBrakeOn = false;
            //temp = 0f;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(BULLET))
        {
            // do shit
            if (currentHealth > 0)
            {
                OnEnemyAttack();
            }
        }
    }

    public void OnEnemyAttack()
    {
        StartCoroutine(OnHitScreenEffect());
        currentHealth--;
        // show hit effect
        if (currentHealth<=0)
        {
            //player is dead
            //Debug.LogError("Player dead");
            analytics.LevelFailed();
            gameController.GetUIController().RestartPanel().SetActive(true);
        }
        else
        {
            //show damage effects
        }
    }

    void Buttons()
    {
        InputManager.instance.onSwipe += delegate (Dir dir)
        {
            Swipe(dir);
        };
    }
    
    private void Swipe(Dir dir)
    {
        //Debug.Log(dir);
        switch (dir)
        {
            case Dir.UP:
                //ForcePush();
                break;
            case Dir.DOWN:
                FrontSlash();
                break;
            case Dir.LEFT:
                LeftSlash();
                break;
            case Dir.RIGHT:
                RightSlash();
                break;
            default:
                break;
        }
    }
    void LeftSlash() { animator.SetTrigger(SLASHL); DeflectBullet(); lightsaber1.PlaySwingSound();  }

    void RightSlash() { animator.SetTrigger(SLASHR); DeflectBullet(); lightsaber1.PlaySwingSound(); }

    void FrontSlash() { animator.SetTrigger(SLASHF); DeflectBullet(); lightsaber1.PlaySwingSound(); }

    void DeflectBullet()
    {
        SaberCollideOn();
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, deflectRadius);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag(BULLET))
            {
                //Debug.Log("found something");
                deflect.transform.position = hitCollider.transform.position;
                deflect.Play();
                hitCollider.GetComponent<Bullet>().KillShooter();
            }
        }
    }

    void ForcePush()
    {
        animator.SetTrigger(FORCEPUSH);
        forcePush.Play();
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, pushRadius);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag(BULLET))
            {
                //Debug.Log("found something");
                deflect.transform.position = hitCollider.transform.position;
                deflect.Play();
                hitCollider.GetComponent<Bullet>().KillShooter();
            }
            else if (hitCollider.CompareTag(ENEMY))
            {
                //Debug.Log("found something");
                hitCollider.GetComponent<EnemyController>().ForcePushed();
            }
        }
    }
    
    
    public void SaberSlam()
    {
        StartCoroutine(SaberSlamRoutine());
    }
    IEnumerator SaberSlamRoutine()
    {
        animator.SetTrigger(SLASHF);
        saberSlam.Play();
        earthShatter.Play();
        yield return SLAMWAIT;
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, slamRadius );
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag(BULLET))
            {
                //Debug.Log("found something");
                deflect.transform.position = hitCollider.transform.position;
                deflect.Play();
                hitCollider.GetComponent<Bullet>().KillShooter();
            }
            else if (hitCollider.CompareTag(ENEMY))
            {
                //Debug.Log("found something");
                hitCollider.GetComponent<EnemyController>().ForceSlamed();
            }
        }
        
        
    }
    void SaberCollideOn()
    {
        StartCoroutine(SaberColliderEnable());
    }
    IEnumerator SaberColliderEnable()
    {
        saberCollider1.enabled = true;
        saberCollider2.enabled = true;
        lightsaberTrail1.time = 0.1f;
        lightsaberTrail2.time = 0.1f;
        yield return WAITONE;
        saberCollider1.enabled = false;
        saberCollider2.enabled = false;
        lightsaberTrail1.time = 0.01f;
        lightsaberTrail2.time = 0.01f;
    }

    IEnumerator OnHitScreenEffect()
    {
        vignette.active = true;
        yield return WAITONE;
        vignette.active = false;
    }

    public void ChangeWielding(WeildingStyle style)
    {
        switch (style)
        {
            case WeildingStyle.ONEHANDED:
                SLASHL = NA_SH_RS;
                SLASHR = NA_SH_LS;

                IDLE = ONEHANDEDIDLE;
                saber1.gameObject.SetActive(false);
                break;
            case WeildingStyle.TWOHANDED:
                SLASHL = NA_TH_RS;
                SLASHR = NA_TH_LS;

                IDLE = TWOHANDEDIDLE;
                saber1.gameObject.SetActive(false);
                break;
            case WeildingStyle.DUALWEILD:
                SLASHL = DW_LS;
                SLASHR = DW_RS;

                IDLE = DW_IDLE;
                saber1.gameObject.SetActive(true);
                break;
            default:
                break;
        }
        //==========

        animator.SetTrigger(IDLE);
    }

    void CameraShake(float value)
    {
        if (Mathf.Abs(value - horizontalValue) > 30f )
        {
            if (value > horizontalValue)
            {
                //right
                cameraAnimator.SetTrigger(RIGHT);
                Debug.Log("joyStick:right " + value);
            }
            else if (value < horizontalValue)
            {
                cameraAnimator.SetTrigger(LEFT);
                Debug.Log("joyStick:left " + value);
            }
            else if (value == horizontalValue)
            {
                Debug.Log("joyStick:default " + value);
            }
        }
    }
    
}

public enum WeildingStyle
{
    ONEHANDED,
    TWOHANDED,
    DUALWEILD
}
